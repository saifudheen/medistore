In this Tutorial
Installing the Client Tools
Setting up Your Machine
Creating an Application
Making Your First Change
Remote Access
Next Steps

Installing the Client Tools on Windows
Installing the client tools (rhc) on Windows requires three steps:

Step 1: Install Ruby with RubyInstaller
Step 2: Install Git version control
Step 3: Install the rhc Ruby gem

Before you can install the rhc client tools, you must download and install Ruby and Git on your system.
Sufficient privileges are required to install software on Windows systems. Depending on specific user permissions, disabling the User Account Control (UAC) on Windows Vista or Windows 7 operating systems may be necessary.
Step 1 - Install Ruby

RubyInstaller provides the best experience for installing Ruby on Windows.

The Client Tools are known to work well with Ruby versions 1.9.3 and 2.0.0 on Windows, based on the community feedback. We recommend downloading and installing one of those versions.
